package sample;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Controller implements Initializable {
    @FXML
    private Label statusLabel;
    @FXML
    private ProgressBar progressBar;


    @FXML
    public void getButtonAction(ActionEvent actionEvent) {
        ExecutorService executor = Executors.newSingleThreadExecutor();//single thread foreach app running
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(null);//choosing file
        if (file == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("You have not chosen any file!");
            alert.setContentText("Please select proper file to send.");
            alert.showAndWait();
            return;
        }

        Task<Void> sendFileTask = new SendFileTask(file);//task and interface contents init
        statusLabel.textProperty().bind(sendFileTask.messageProperty());
        progressBar.progressProperty().bind(sendFileTask.progressProperty());
        executor.submit(sendFileTask);
        executor.shutdownNow();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        progressBar.setProgress(0);
    }

}
