package sample;

import javafx.concurrent.Task;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.Socket;

public class SendFileTask extends Task<Void> {
    private File file;

    private File getFile() {
        return file;
    }

    public SendFileTask(File file) {
        this.file = file;
    }

    @Override
    protected Void call() throws Exception {
        updateMessage("Initiating...");
        updateProgress(0, this.getFile().length());

        try (Socket socket = new Socket("127.0.0.1", 17209)) {//socket init
            try (DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream())) {//output stream init
                dataOutputStream.writeUTF(this.getFile().getName());
                dataOutputStream.writeLong(this.getFile().length());
                try (FileInputStream fileInputStream = new FileInputStream(this.getFile())) {//input stream init
                    long fileSize = this.getFile().length();
                    long bytesSend = 0;
                    byte[] buffer = new byte[16]; //buffer init
                    updateMessage("File upload in progress...");

                    while (bytesSend != fileSize) { //upload loop
                        int readSize = fileInputStream.read(buffer);
                        if (readSize != -1) {
                            bytesSend += readSize;
                        }
                        dataOutputStream.write(buffer, 0, readSize);
                        updateProgress(bytesSend, this.getFile().length());
                    }
                }
            }
        } catch (Exception e) {
            updateMessage(e.getMessage());
        }

        updateMessage("Done!");
        updateProgress(100, 100);

        Thread.sleep(2000); //waiting 2s before updating the progress and message

        updateMessage("Please choose the file to upload");
        updateProgress(0, 100);

        return null;
    }
}
