import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerMain {
    private static int port = 17209;

    private static int getPort() {
        return port;
    }

    private static void receiveFiles(Socket socket) throws Exception {
        String fileName;
        long fileSize;
        long bytesRead = 0;
        try (DataInputStream dataInputStream = new DataInputStream(socket.getInputStream())) { //input stream init
            fileName = dataInputStream.readUTF();
            fileSize = dataInputStream.readLong();
            File file = new File("./Uploads", fileName);
            file.createNewFile();
            try (FileOutputStream fileOutputStream = new FileOutputStream(file)) { //output stream init
                byte[] buffer = new byte[1024];//buffer init
                System.out.println("Starting download of "+fileName);
                while (bytesRead != fileSize) {//download loop
                    int recentlyRead = dataInputStream.read(buffer);
                    if (recentlyRead != -1) { //check if the part of file is able to read
                        bytesRead += recentlyRead;
                    } else {
                        throw new Exception("Part of the file might be broken.");
                    }
                    fileOutputStream.write(buffer, 0, recentlyRead);
                }

                System.out.println("Download of " + fileName + " finished succesfully");
            }
        } catch (Exception e) {
            System.out.println("There was an error with the download.");
        }

        socket.close(); //closing socket
    }

    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(4);//static number of threads for clients

        try {
            ServerSocket serverSocket = new ServerSocket(ServerMain.getPort());//server socket init
            while (true) {
                final Socket socket = serverSocket.accept();
                executorService.submit(() -> {
                    try {
                        ServerMain.receiveFiles(socket);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            executorService.shutdownNow();
        }
    }

}

